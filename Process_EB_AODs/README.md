# bbtautautriggeranalysis

### Build and compile 
```
source athsetupr22.sh
```
**For future logins, do:** `source athloginr22.sh`

### Run joboptions 
```
cd run/

#For local run, do:
source localrun.sh

#For grid run, do:
source gridrun.sh
```

### Datasets

Sample AOD file: `/afs/cern.ch/user/a/asudhaka/public/AOD.33426411._000001.pool.root.1`

Note: You may have to edit the file directory in the `../source/MyAnalysis/share/ATestRunLocal_jobOptions.py` to get it running 
