#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

testFile0 = '/afs/cern.ch/user/a/asudhaka/eos_space/tau_trigger/mc23_13p6TeV.601477.PhPy8EG_HHbbttHadHad_cHHH01d0.recon.AOD.e8514_e8528_s4111_s4114_r14668/AOD.33514441._000001.pool.root.1'
testFile1 = '/afs/cern.ch/user/a/asudhaka/eos_space/tau_trigger/mc23_13p6TeV.601477.PhPy8EG_HHbbttHadHad_cHHH01d0.recon.AOD.e8514_e8528_s4111_s4114_r14668/AOD.33514441._000002.pool.root.1'

#override next line on command line with: --filesInput=
# jps.AthenaCommonFlags.FilesInput = [testFile0] #
jps.AthenaCommonFlags.FilesInput = [testFile0, testFile1]

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:kl01_mc23.root"]
svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# First create all the public tools to be used in the job
from AthenaCommon.AppMgr import ToolSvc
from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
import AthenaCommon.CfgMgr as CfgMgr

ToolSvc += CfgMgr.TrigConf__xAODConfigTool("xAODConfigTool")
ToolSvc += CfgMgr.Trig__TrigDecisionTool(
        "TrigDecisionTool",
        ConfigTool = ToolSvc.xAODConfigTool,
        TrigDecisionKey = "xTrigDecision",
        NavigationFormat = "TrigComposite",
        HLTSummary="HLTNav_Summary_AODSlimmed") 
        # NavigationFormat = "TrigComposite" 

from TrigEDMConfig.TriggerEDM import EDMLibraries
ToolSvc.TrigDecisionTool.Navigation.Dlls = [e for e in EDMLibraries if 'TPCnv' not in e]

ToolSvc += CfgMgr.TauAnalysisTools__TauTruthMatchingTool("TauTruthMatchingTool")

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
#alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
alg = CfgMgr.MyxAODAnalysis(
    'AnalysisAlg',
    TrigDecisionTool = ToolSvc.TrigDecisionTool, 
    TauTruthMatchingTool = ToolSvc.TauTruthMatchingTool
    )

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
# theApp.EvtMax = 500

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

