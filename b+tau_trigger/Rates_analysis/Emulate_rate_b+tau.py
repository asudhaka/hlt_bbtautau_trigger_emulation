#########################################################################################################################################################
# IMPORTS #
#########################################################################################################################################################

import ROOT
import pandas as pd
import numpy as np
import csv
from Trigger_functions import *

#########################################################################################################################################################
# PREPARING THE DATA #
#########################################################################################################################################################

print('Loading the root file...')

branches_to_select = ['HLTJets','HLTJets_GN1b','HLTJets_GN1c', 'HLTJets_GN1u', 'HLTJets_Jvt','L1_4b_3j1j','L1_trig_pt8','TrigTRM_TauIDl', 'TrigTRM_TauIDm','TrigTRM_TauIDt', 'TrigTRM_Taus', 'TrigTRM_prong','TrigTRM_rnn']
# Your Enhnace bias NTuples
# rdf = ROOT.RDataFrame('analysis', 'path/to/your/NTuples.root')
rdf1 = ROOT.RDataFrame('analysis', "/afs/cern.ch/user/a/asudhaka/eos_space/user.asudhaka.35269222.ANALYSIS._000001.root")

# THe weights of your Enhanced Bias NTuples
# rdf2 = ROOT.RDataFrame('trig', "path/to/your/weights.root")
rdf2 = ROOT.RDataFrame('trig', "/afs/cern.ch/user/a/asudhaka/eos_space/user.asudhaka.35269222.ANALYSIS._000001.EBweights.root")

# Convert the RDataFrame to a pandas DataFrame (Memory intensive !!! careful with the root file size)
analysis_tree = rdf1.AsNumpy(columns=branches_to_select)
weight_tree = rdf2.AsNumpy(columns=['EBweight'])
df1 = pd.DataFrame(analysis_tree)
df2 = pd.DataFrame(weight_tree)

# Make a dataframe with all the variables required for analysis
df = pd.concat([df1, df2], axis=1)

# To convert c++ boolean dataformat of a certain column to python readable (Some problemm with this column, usually not required)
df['TrigTRM_TauIDm'] = df['TrigTRM_TauIDm'].apply(lambda x: [bool(item) for item in x])
df['TrigTRM_TauIDl'] = df['TrigTRM_TauIDl'].apply(lambda x: [bool(item) for item in x])
df['TrigTRM_TauIDt'] = df['TrigTRM_TauIDt'].apply(lambda x: [bool(item) for item in x])

print('Loading dataframe successful! Now emulating triggers...')

# Weighted number of events that pass the L1 chain of b+tau for selection

sel_pass = []
sel_chain_weights = 0
for event in range(df.shape[0]):
    if df['L1_4b_3j1j'][event] == True:
        sel_pass.append(event)
        sel_chain_weights += df['EBweight'][event]


# Rate and the error of the idperf L1Topo chain from reprocessing page which is
# @ https://atlas-trig-cost.web.cern.ch/?dir=RateProcessings-2023&type=r2023-05-10&tag=rate-prediction-ATR-27517-noPS&run=440499&range=All&level=HLT&summary=Rate_ChainHLT
# All the rates are determined with a luminosity of 1.00e+34 cm^2s^-1Rate_HLT_L1Topo_idperf = 2768.7083 # Hz

Rate_sel_chain = 4429.6122 # Hz
Err_Rate_sel_chain = 22.8332 # Hz

#########################################################################################################################################################
# Efficiency emulation formula #
#########################################################################################################################################################

# Rate_emu = N_event_passed_emu * Rate_std_chain / N_event_passed_std

##############################################################
####### Customize your emulation parameter range here ########
##############################################################

gn1s = ['gn185']
tpts = [20]
tau_IDs = ['medium'] 
bpt0s = [100]
bpt1s = [70]
bpt2s = [30]
bpt3s = [20,25,30]

# gn1s = ['gn170','gn177','gn185']
# tpts = [35,30,25,20]
# tau_IDs = ['medium','tight'] 
# bpt0s = [40, 60, 75, 80, 100]
# bpt1s = [30, 40, 50, 60, 70]
# bpt2s = [20, 25, 30]
# bpt3s = [20, 25, 30]

# To determine the total trigger combinations to be emulated
t_count = 0
for gn1 in gn1s:
    for tpt in tpts:
        for tau_ID in tau_IDs:
            for bpt0 in bpt0s:
                for bpt1 in bpt1s:
                    for bpt2 in bpt2s:
                        for bpt3 in bpt3s:                  
                            if bpt0 >= bpt1 and bpt1 >= bpt2 and bpt2 >= bpt3:
                                t_count+=1

##############################################################

#########################################################################################################################################################
# EMULATION #
#########################################################################################################################################################

count = 0
eff = 0

count=0
rate=0
with open('rate_studies.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['tau_pt','tau_ID','j0_pt','j1_pt','j2_pt','j3_pt','gn1_wp','Events_passed','Rate','Err_Rate'])
    for gn1 in gn1s:
        for tpt in tpts:
            for tau_ID in tau_IDs:
                for bpt0 in bpt0s:
                    for bpt1 in bpt1s:
                        for bpt2 in bpt2s:
                            for bpt3 in bpt3s:                  
                                if bpt0 > bpt1 and bpt1 > bpt2 and bpt2 > bpt3:
                                    hlt_events = 0
                                    hlt_events_wt = 0
                                    hlt_event_wt_err = 0
                                    print(f'{count} combination(s) completed of {t_count-1} |   Previous rate: {rate}   |  Current combination => tau with {tpt}, {tau_ID} and jets with {bpt0,bpt1,bpt2,bpt3} and {gn1}',end='', flush=True)
                                    for event in sel_pass:
                                        if pass_3b1j(df,event, pt_tau=tpt, cond=tau_ID, pt_j0=bpt0, pt_j1=bpt1, pt_j2=bpt2, pt_j3=bpt3, jvt=0.2, b_WP_tag=gn1):
                                            hlt_events_wt += df['EBweight'][event]
                                            hlt_event_wt_err += df['EBweight'][event]**2
                                            
                                    rate = hlt_events_wt * (Rate_sel_chain/sel_chain_weights)
                                    rate_err = (((hlt_events_wt/sel_chain_weights)*Err_Rate_sel_chain)**2 + (np.sqrt(hlt_event_wt_err) * (Rate_sel_chain/sel_chain_weights))**2)**0.5
                                    writer.writerow([str(tpt), tau_ID, str(bpt0), str(bpt1), str(bpt2), str(bpt3), gn1, hlt_events_wt, rate, rate_err])   
                                    count+=1
                                    print('\r \r', end='', flush=True)
print(f'{count} combination(s) completed of {t_count} |   Rate: {rate}   |  Combination => tau with {tpt}, {tau_ID} and jets with {bpt0,bpt1,bpt2,bpt3} and {gn1}',end='', flush=True)
print('\nEmulation completed Successfully!')