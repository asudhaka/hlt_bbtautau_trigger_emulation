import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerTuple, HandlerPathCollection
import atlas_mpl_style as ampl
plt.figure(dpi=100)

# Choose the rates and efficiency files for the trigger chain under analysis
data1 = pd.read_csv('4J12_Rates_Emulation.csv')
data2 = pd.read_csv('4J12_Efficiency_Emulation.csv')
df = pd.concat([data1, data2[['Eff','Eff_err']]], axis=1)

pm = df['wp-mRNN-mp']
p1 = df['wp-mRNN-1p']
pt1 = df['pt1']
pt0 = df['pt0']
eff = df['Eff']
eff_err = df['Eff_err']
rate = df['Rate']
rate_err = df['Err_Rate']


# Current Trigger Working Points
m0_current = 1-0.65
m1_current =  1-0.97
mm_current = 1-0.895
pt0_current = 30
pt1_current = 20

current_eff = df.loc[(df['pt0'] == pt0_current) & (df['pt1'] == pt1_current) & (df['wp-mRNN-1p'] == np.round(m1_current,4)) & (df['wp-mRNN-mp'] == np.round(mm_current,4)), 'Eff'].values[0]
current_rate = df.loc[(df['pt0'] == pt0_current) & (df['pt1'] == pt1_current) & (df['wp-mRNN-1p'] == np.round(m1_current,4)) & (df['wp-mRNN-mp'] == np.round(mm_current,4)), 'Rate'].values[0]

# To remove points from plot , add here
keep_pt0 = [30,32,34,36,38,40]
keep_pt1 = [20,21,22]

# Set up a colormap for different colors based on pt0 values
color_map = plt.cm.get_cmap('Set2')

# Set up markers for different pt1 values
marker = ['x','o','^','v','s','*']

# Loop through unique pt1 values and plot efficiency vs. rate with different colors and markers
for j, pt1_val in enumerate(keep_pt1):
    for i, pt0_val in enumerate(keep_pt0):
        if (pt0_val in keep_pt0) and (pt1_val in keep_pt1):
            # Filter data points for the current pt1 value
            mask = (pt0 == pt0_val) & (pt1 == pt1_val) #& (eff > current_eff-0.001) & (rate < current_rate+5)
            x = eff[mask]
            y = rate[mask]
            y_err = eff_err[mask]

            # Get a color from the colormap based on the index
            color = color_map(i / len(keep_pt0))

            # Plot the data points with the corresponding color and marker
            plt.scatter(x, y, color=color, marker=marker[j], s=20, label=f'pt1 = {pt1_val:.0f}',alpha=1)


# Create separate legend entries for colors (pt0) and markers (pt1)
colors_legend = [plt.scatter([], [], marker='_',linewidths=4, color=color_map(i / len(keep_pt0)), s=200, label=f'lead = {pt0_val:.0f}') for i, pt0_val in enumerate(keep_pt0)]
markers_legend = [plt.scatter([], [], marker=marker[i], color='black', s=30, label=f'subl = {pt1_val:.0f}') for i, pt1_val in enumerate(keep_pt1)]

# Combine legends using HandlerTuple
handlers = [HandlerTuple(colors_legend), HandlerPathCollection(markers_legend)]

# Add labels, legend, and title
plt.xlabel(r'Efficiency ($\epsilon$)')
plt.ylabel(r'Rate$_{L1Topo}$ ($Hz$) $(\mathcal{L} = 1 \times 10^{34} cm^{-2} s^{-1})$')
plt.legend(loc='lower right', handles=colors_legend + markers_legend, handler_map={tuple: HandlerTuple(ndivide=None)}, fontsize=12,title='pT(GeV)',title_fontsize=15)

plt.plot([current_eff,current_eff],[0,200],color='black',linewidth=0.5)
plt.plot([0,1],[current_rate,current_rate],color='black',linewidth=0.5)

plt.xlim(0.47,0.57)    
plt.ylim(10,95)
plt.tight_layout()

# ampl.plot.draw_atlas_label(x=0.4,y=0.87,status ="Preliminary")    
plt.savefig(f'Rate_vs_Eff.jpg')
